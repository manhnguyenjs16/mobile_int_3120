package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.example.myapplication.Reminder.AlarmReceiver;
import com.example.myapplication.Training.TrainingFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private CardView cardView_training;
    private CardView cardView_reminder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        cardView_training = findViewById(R.id.cardView_Training);
        cardView_reminder = findViewById(R.id.cardView_Reminder);

        cardView_training.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               TrainingFragment fragment = new TrainingFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.main, fragment);
                transaction.commit();
            }
        });

        cardView_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createReminder();
                }

            }
        });


    }

    private void createReminder() {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int y = i, m = i1, d = i2;
                TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {

                    Calendar calendar = Calendar.getInstance();

                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {

                        calendar.set(y, m, d, i, i1 );
                        long reminderTimeInMillis = calendar.getTimeInMillis();
//                        System.out.println(calendar.get(Calendar.DAY_OF_MONTH));
                        createNotification(reminderTimeInMillis);
                    }

                }, hour, minute, true);

                calendar.set(i, i1, i2);
                timePickerDialog.show();


            }
        },year, month, day );

        datePickerDialog.show();
    }

    private void createNotification(long time) {

        createNotificationChanel();
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pendingIntent);




    }

    private void createNotificationChanel() {
        NotificationChannel notificationChannel = new NotificationChannel("channel1", "LearnEnglish!!!", NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.setDescription("Time to watch movie!!");
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);

    }


}